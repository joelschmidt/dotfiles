vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv")

vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<C-u>', '<C-u>zz')

vim.keymap.set('i', '<C-c>', '<Esc>')

vim.keymap.set('n', 'Q', '<nop>')
vim.keymap.set('n', '<C-f>', '<cmd>silent !tmux neww tmux-sessionizer<CR>')
vim.keymap.set('n', '<leader>f', vim.lsp.buf.format)

vim.keymap.set('n', '<leader>x', '<cmd>!chmod +x %<CR>', { silent = true })

vim.keymap.set('n', 'H', vim.cmd.bprevious)
vim.keymap.set('n', 'L', vim.cmd.bnext)
vim.keymap.set('n', '<leader>bp', vim.cmd.bprevious, { desc = '[p] Go to previous buffer' })
vim.keymap.set('n', '<leader>bn', vim.cmd.bnext, { desc = '[n] Go to next buffer' })
vim.keymap.set('n', '<leader>bd', vim.cmd.bdelete, { desc = '[d] Delete current buffer' })
