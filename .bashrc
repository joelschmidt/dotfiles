#
# ~/.bashrc
#

### EXPORT
export TERM="xterm-256color"                      # getting proper colors
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries
export EDITOR="nvim"              		  # $EDITOR use Emacs in terminal
export VISUAL="code"           			  # $VISUAL use VS Code

### SET MANPAGER
### "bat" as manpager
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

### PATH
if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/apps" ] ;
  then PATH="$HOME/apps:$PATH"
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '


[[ -f ~/.bashrc_aliases ]] && . ~/.bashrc_aliases
[[ -f ~/.bashrc_functions ]] && . ~/.bashrc_functions


fastfetch

# Oh-My-Posh Config
eval "$(oh-my-posh init bash --config $HOME/.cache/oh-my-posh/themes/capr4n.omp.json)"

eval "$(zoxide init bash)"
