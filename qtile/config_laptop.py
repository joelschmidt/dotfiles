import os
import re
import socket
import subprocess
from typing import List  # noqa: F401
from libqtile import layout, bar, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen, Rule
from libqtile.command import lazy
from libqtile.widget import Spacer
# Make sure 'qtile-extras' is installed or this config will not work.
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration

#mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser('~')

myTerm = "alacritty"
myBrowser = "qutebrowser"
myExplorer = "thunar"
myEmacs = "emacsclient -c -a 'emacs' " # The space at the end is IMPORTANT!


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


# COLORS FOR THE BAR
#Theme name : ArcoLinux Default
def init_default_colors():
    return [["#2F343F", "#2F343F"], # color 0
            ["#2F343F", "#2F343F"], # color 1
            ["#c0c5ce", "#c0c5ce"], # color 2
            ["#fba922", "#fba922"], # color 3
            ["#3384d0", "#3384d0"], # color 4
            ["#f3f4f5", "#f3f4f5"], # color 5
            ["#cd1f3f", "#cd1f3f"], # color 6
            ["#62FF00", "#62FF00"], # color 7
            ["#6790eb", "#6790eb"], # color 8
            ["#a9a9a9", "#a9a9a9"]] # color 9


#Theme name : ArcoLinux Dracula
def init_dracula_colors():
    return [["#000000", "#000000"], # color 0
            ["#282A36", "#282A36"], # color 1
            ["#F8F8F2", "#F8F8F2"], # color 2
            ["#F1FA8C", "#F1FA8C"], # color 3
            ["#FF5555", "#FF5555"], # color 4
            ["#D8DEE9", "#D8DEE9"], # color 5
            ["#8BE9FD", "#8BE9FD"], # color 6
            ["#BFBFBF", "#BFBFBF"], # color 7
            ["#4D4D4D", "#4D4D4D"], # color 8
            ["#BD93F9", "#BD93F9"]] # color 9


def init_doomone_colors():
    return [["#3B4252", "#3B4252"],
            ["#282c34", "#282c34"], # bg
            ["#bbc2cf", "#bbc2cf"], # fg
            ["#1c1f24", "#1c1f24"], # color01
            ["#ff6c6b", "#ff6c6b"], # color02
            ["#D8DEE9", "#D8DEE9"], # color03
            ["#da8548", "#da8548"], # color04
            ["#51afef", "#51afef"], # color05
            ["#c678dd", "#c678dd"], # color06
            ["#46d9ff", "#46d9ff"]]  # color15


#Theme name : JS Theme
def init_js_colors():
    return [["#3B4252", "#3B4252"], # color 0
            ["#2F343F", "#2F343F"], # color 1 - Bar Background
            ["#A3BE8C", "#A3BE8C"], # color 2 - Separators
            ["#EBCB8B", "#EBCB8B"], # color 3 - Icon Color (yellow/orange)
            ["#BD93F9", "#BD93F9"], # color 4 - Accent Color (dmenu, rofi, Active (Current) workspace)
            ["#D8DEE9", "#D8DEE9"], # color 5 - Inactive/Empty workspace
            ["#88C0D0", "#88C0D0"], # color 6
            ["#E5E9F0", "#E5E9F0"], # color 7
            ["#4C566A", "#4C566A"], # color 8
            ["#6790EB", "#6790EB"]] # color 9 - Active (Not Current) workspace


colors = init_js_colors()
#colors = init_doomone_colors()
#colors = init_dracula_colors()


keys = [

    Key([mod2, "shift"], "Escape", lazy.spawn("xfce4-taskmanager")),

# SUPER + FUNCTION KEYS

    Key([mod], "Return", lazy.spawn(myTerm)),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),
    #Key([mod], "x", lazy.spawn("archlinux-logout")),
    Key([mod], "x", lazy.spawn("loginctl terminate-user {0}".format(os.environ["USER"]))),
    Key([mod], "r", lazy.spawn("rofi-theme-selector")),
    Key([mod], "t", lazy.spawn("archlinux-tweak-tool")),
    Key([mod], "v", lazy.spawn("pavucontrol")),
    Key([mod], "w", lazy.spawn("arcolinux-welcome-app")),
    Key([mod], "b", lazy.spawn(myBrowser)),
    Key([mod], "c", lazy.spawn("chromium -no-default-browser-check")),
    Key([mod], "e", lazy.spawn(myExplorer)),
    Key([mod], "m", lazy.spawn("sudo mount.cifs //192.168.86.111/media ~/omv_share -o username=joel,password=smilee21")),


# SUPER + SHIFT KEYS

    Key([mod, "shift"], "Return", lazy.spawn("rofi -show drun")),
    Key([mod, "shift"], "d", lazy.spawn("dmenu_run -i -nb '" + colors[1][0] + "' -nf '" + colors[5][0] + "' -sb '" + colors[4][0] + "' -sf '#191919' -fn 'Noto Sans:normal:pixelsize=16'")), # 'NotoMonoRegular:normal:pixelsize=19'")),
#    Key([mod, "shift"], "e", lazy.spawn("emacsclient -c -a 'emacs'")),
    Key([mod, "shift"], "q", lazy.window.kill()),
    Key([mod, "shift"], "r", lazy.restart()),
    Key([mod, "shift"], "s", lazy.spawn("flameshot gui")),
    Key([mod, "shift"], "v", lazy.spawn("volumeicon")),


# QTILE LAYOUT KEYS
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),

# CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),


# RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),


# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),

# FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),

# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),

# TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),

# Dmenu scripts launched using the key chord SUPER+p followed by 'key'
    KeyChord([mod], "p", [
        Key([], "h", lazy.spawn("dm-hub"), desc='List all dmscripts'),
        Key([], "a", lazy.spawn("dm-sounds"), desc='Choose ambient sound'),
        Key([], "b", lazy.spawn("dm-setbg"), desc='Set background'),
        Key([], "c", lazy.spawn("dtos-colorscheme"), desc='Choose color scheme'),
        Key([], "e", lazy.spawn("dm-confedit"), desc='Choose a config file to edit'),
        Key([], "i", lazy.spawn("dm-maim"), desc='Take a screenshot'),
        Key([], "k", lazy.spawn("dm-kill"), desc='Kill processes '),
        Key([], "m", lazy.spawn("dm-man"), desc='View manpages'),
        Key([], "n", lazy.spawn("dm-note"), desc='Store and copy notes'),
        Key([], "o", lazy.spawn("dm-bookman"), desc='Browser bookmarks'),
        Key([], "p", lazy.spawn("passmenu -p \"Pass: \""), desc='Logout menu'),
        Key([], "q", lazy.spawn("dm-logout"), desc='Logout menu'),
        Key([], "r", lazy.spawn("dm-radio"), desc='Listen to online radio'),
        Key([], "s", lazy.spawn("dm-websearch"), desc='Search various engines'),
        Key([], "t", lazy.spawn("dm-translate"), desc='Translate text')
    ])
]

def window_to_previous_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen == True:
            qtile.cmd_to_screen(i - 1)

def window_to_next_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen == True:
            qtile.cmd_to_screen(i + 1)

keys.extend([
    # MOVE WINDOW TO NEXT SCREEN
    Key([mod,"shift"], "Right", lazy.function(window_to_next_screen, switch_screen=True)),
    Key([mod,"shift"], "Left", lazy.function(window_to_previous_screen, switch_screen=True)),
])

groups = []
group_names = ["1", "2", "3", "4", "5", "6"]

#group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0",]
#group_labels = ["", "", "", "", "", "", "", "", "", ""]
#group_labels = ["Web", "Edit", "Chat", "Files", "Media", "Other"]
#group_labels = ["", "", "", "", "", ""]

#group_labels = ["", "", "", "", "", ""]
group_labels = ["", "", "", "", "", ""]
group_layouts = ["monadtall", "monadtall", "ratiotile", "monadtall", "monadtall", "monadtall"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Tab", lazy.screen.next_group()),
        Key([mod, "shift" ], "Tab", lazy.screen.prev_group()),
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])


def init_layout_theme():
    return {"margin":5,
            "border_width":2,
            "border_focus": "#5e81ac",
            "border_normal": "#4c566a"
            }

layout_theme = init_layout_theme()


layouts = [
    #layout.MonadTall(margin=8, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    layout.MonadTall(**layout_theme),
    #layout.MonadWide(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    #layout.Max(**layout_theme)
]


# WIDGETS FOR THE BAR
def init_widgets_defaults():
    return dict(font="Noto Sans",
                fontsize = 12,
                padding = 3,
                background=colors[1])

widget_defaults = init_widgets_defaults()

def init_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [
               widget.GroupBox(font="FontAwesome",
                        fontsize = 16,
                        margin_y = 3,
                        margin_x = 0,
                        padding_y = 6,
                        padding_x = 5,
                        borderwidth = 0,
                        disable_drag = True,
                        active = colors[9],
                        inactive = colors[5],
                        rounded = False,
                        highlight_method = "text",
                        this_current_screen_border = colors[4],
                        foreground = colors[5],
                        background = colors[1]
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[4],
                        background = colors[1]
                        ),
               widget.CurrentLayoutIcon(
                        foreground = colors[1],
                        padding = 0,
                        scale = 0.7
                        ),
                widget.Sep(
                         linewidth = 1,
                         padding = 10,
                         foreground = colors[4],
                         background = colors[1]
                         ),
               widget.WindowName(font="Noto Sans",
                        fontsize = 12,
                        foreground = colors[5],
                        background = colors[1],
                        ),
#                widget.GenPollText(
#                        update_interval = 300,
#                        func = lambda: subprocess.check_output("printf $(uname -r)", shell=True, text=True),
#                        foreground = colors[3],
#                        fmt = '  {}',
#                        decorations=[
#                            BorderDecoration(
#                                colour = colors[3],
#                                border_width = [0, 0, 2, 0],
#                            )
#                        ],
#                        ),
#                widget.Spacer(length = 8),
#                widget.CPU(
#                        format = '▓  Cpu: {load_percent}%',
#                        foreground = colors[3],
#                        decorations=[
#                            BorderDecoration(
#                                colour = colors[3],
#                                border_width = [0, 0, 2, 0],
#                            )
#                        ],
#                        ),
#                widget.Spacer(length = 8),
#                widget.Memory(
#                        foreground = colors[9],
#                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
#                        format = '{MemUsed: .0f}{mm}',
#                        fmt = '🖥  Mem: {} ',
#                        decorations=[
#                            BorderDecoration(
#                                colour = colors[9],
#                                border_width = [0, 0, 2, 0],
#                            )
#                        ],
#                        ),
#                widget.Spacer(length = 8),
                widget.DF(
                        update_interval = 60,
                        foreground = colors[2],
                        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e df')},
                        partition = '/',
                        #format = '[{p}] {uf}{m} ({r:.0f}%)',
                        format = '{uf}{m} free',
                        fmt = '🖴  Disk: {}',
                        visible_on_warn = False,
                        decorations=[
                            BorderDecoration(
                                colour = colors[2],
                                border_width = [0, 0, 2, 0],
                            )
                        ],
                        ),
#                widget.Spacer(length = 8),
#                widget.Volume(
#                        foreground = colors[4],
#                        fmt = '🕫  Vol: {}',
#                        decorations=[
#                            BorderDecoration(
#                                colour = colors[4],
#                                border_width = [0, 0, 2, 0],
#                            )
#                        ],
#                        ),
                widget.Spacer(length = 8),
                widget.Clock(
                        foreground = colors[4],
                        background = colors[1],
                        fontsize = 12,
#                        format="%Y-%m-%d - %H:%M",
#                        format = "⏱  %a, %b %d - %H:%M",
                        format = " %a, %b %d - %H:%M",
                        decorations=[
                            BorderDecoration(
                                colour = colors[4],
                                border_width = [0, 0, 2, 0],
                            )
                        ],
                        ),
                widget.Spacer(length = 8),
                widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[4],
                        background = colors[1]
                        ),
                widget.CheckUpdates(
                        font="FontAwesome",
                        custom_command='checkupdates',
                        no_update_string="  ",
                        display_format='{updates} '
                        ),
                widget.Systray(
                        background=colors[1],
                        icon_size=20,
                        padding = 4
                        ),
                widget.Spacer(length = 8),
              ]
    return widgets_list

widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2

widgets_screen1 = init_widgets_screen1()
# widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26, opacity=0.8))]
#            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26, opacity=0.8))]
screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

dgroups_key_binder = None
#dgroups_app_rules:List[] = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN

#########################################################
################ assign apps to groups ##################
#########################################################
@hook.subscribe.client_new
def assign_app_group(client):
    d = {}
#     #####################################################################################
#     ### Use xprop fo find  the value of WM_CLASS(STRING) -> First field is sufficient ###
#     #####################################################################################
#    d[group_names[0]] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser",
#               "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", ]
#     d[group_names[1]] = [ "Atom", "Subl", "Geany", "Brackets", "Code-oss", "Code", "TelegramDesktop", "Discord",
#                "atom", "subl", "geany", "brackets", "code-oss", "code", "telegramDesktop", "discord", ]
#    d[group_names[1]] = ["chromium"]
#     d[group_names[2]] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh",
#               "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
    d[group_names[2]] = ["Signal", "signal", "crx_hpfldicfbfomlpcikngkocigghgafkph" ]
#     d[group_names[3]] = ["Gimp", "gimp" ]
    d[group_names[4]] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
               "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
#     d[group_names[4]] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
#     d[group_names[5]] = ["Vlc","vlc", "Mpv", "mpv" ]
#     d[group_names[6]] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
#               "virtualbox manager", "virtualbox machine", "vmplayer", ]
#    d[group_names[7]] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
#               "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
#     d[group_names[8]] = ["Evolution", "Geary", "Mail", "Thunderbird",
#               "evolution", "geary", "mail", "thunderbird" ]
#     d[group_names[9]] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
#               "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
#     ######################################################################################
#
    wm_class = client.window.get_wm_class()[0]
#
    for i in range(len(d)):
        if wm_class in list(d.values())[i]:
            group = list(d.keys())[i]
            client.togroup(group)
            client.group.cmd_toscreen(toggle=False)

# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME



main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    bar.window.window.set_property("QTILE_BAR", 1, "CARDINAL", 32)
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    border_focus=colors[9],
    border_width=2,
    fullscreen_border_width = 0,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class='confirmreset'),  # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(wm_class='ssh-askpass'),  # ssh-askpass
        Match(wm_class='Arcolinux-welcome-app.py'),
        Match(wm_class='Arcolinux-calamares-tool.py'),
        Match(wm_class='confirm'),
        Match(wm_class='dialog'),
        Match(wm_class='download'),
        Match(wm_class='error'),
        Match(wm_class='file_progress'),
        Match(wm_class='notification'),
        Match(wm_class='splash'),
        Match(wm_class='toolbar'),
        Match(wm_class='Arandr'),
        Match(wm_class='feh'),
        Match(wm_class='Galculator'),
        Match(wm_class='archlinux-logout'),
        Match(wm_class='xfce4-terminal'),
        Match(wm_class='kdenlive'),       # kdenlive
        Match(title="branchdialog"),      # gitk
        Match(title='Confirmation'),      # tastyworks exit box
        Match(title='Qalculate!'),        # qalculate-gtk
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart" # smart or focus
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

wmname = "LG3D"

